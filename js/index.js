function windowResized() {
    resizeCanvas(windowWidth-20, windowHeight-20);
}

var props = {
    innerRad: 15,
    outerRad: 215,
    strichst: 100,
    radStep: 20,
    ringOffset: 0.1,
    noiseEffect: 0.6,
    speed: 6,
    opacity: 0.5,
    hintergr: "#141414",
    fade: 0.1,
    o1: "txpix woody",
    o2: " ",
    ende: "#9abeb9",
    anfang: "#f85447",
}

window.onload = function() {
    var gui = new dat.GUI();
    gui.remember(props);
    gui.add(props, 'o1');
    gui.add(props, 'o2');
    gui.close();
};

var wanderHue = true;

function setup() {
    createCanvas(windowWidth-20, windowHeight-20);
    smooth();
    frameRate(25);
    strokeWeight(0);
    noFill();
    pixelDensity(1);
}

function rgbToHex(r, g, b) {
    return "#" + componentToHex(r) + componentToHex(g) + componentToHex(b);
}

function hexToRgb(hex) {
    var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
    return result ? {
        r: parseInt(result[1], 16),
        g: parseInt(result[2], 16),
        b: parseInt(result[3], 16)
    } : null;
}

function intex(start, end, steps, count) {
    var s = start,
        e = end,
        final = s + (((e - s) / steps) * count);
    return Math.floor(final);
}

function draw() {

    colorMode(RGB, 255, 255, 255, 1);
    var col = color(props.hintergr);
    fill(color(red(col), green(col), blue(col), 1.0 - props.fade));
    rect(0, 0, width, height);
    noStroke();
    noFill();

    if (wanderHue) {
        props.hueSpread += (noise(frameCount / 300.0) - 0.5) * 0.01;
    }

    translate(width / 2, height / 2);

    colorMode(RGB, 255, 255, 255, 1);
    var offset = 0;

    //background
    var oldR = hexToRgb(props.anfang).r;
    var oldG = hexToRgb(props.anfang).g;
    var oldB = hexToRgb(props.anfang).b;
    //front
    var newR = hexToRgb(props.ende).r;
    var newG = hexToRgb(props.ende).g;
    var newB = hexToRgb(props.ende).b;

    var r = new Array();
    for (var i = 0; i < props.radStep; i++) {
        currentR = intex(oldR, newR, props.radStep, i);
        r.push(currentR);
    }

    var g = new Array();
    for (var i = 0; i < props.radStep; i++) {
        currentG = intex(oldG, newG, props.radStep, i);
        g.push(currentG);
    }

    var b = new Array();
    for (var i = 0; i < props.radStep; i++) {
        currentB = intex(oldB, newB, props.radStep, i);
        b.push(currentB);
    }



    for (var rad = props.innerRad; rad < props.outerRad; rad += props.radStep) {
        var i = offset * props.radStep;

        strokeWeight(props.strichst);
        stroke(r[i], g[i], b[i], props.opacity);
        blobCircle(rad, offset);
        offset += props.ringOffset;
        noStroke();
    }

}

function blobCircle(rad, ranOff) {

    beginShape();
    var step = 2;
    if (props.radStep < 8)
        step = 8;

    for (var i = 0; i < 360; i += step) {
        var x = sin(radians(i));
        var y = cos(radians(i));

        var frameDiv = map(props.speed, 0, 10, 300, 24);

        x += props.noiseEffect * (noise(ranOff + x + frameCount / frameDiv) - 0.5);
        y += props.noiseEffect * (noise(ranOff + y + frameCount / frameDiv) - 0.5);

        x *= rad;
        y *= rad;

        vertex(x, y);
    }
    endShape(CLOSE);
}

function componentToHex(c) {
    var hex = c.toString(16);
    return hex.length == 1 ? "0" + hex : hex;
}

function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1) + min);
}

// braucht aktuelle farben

var fancynancy = ["#3ad398", "#ff5b4c", "#ffa04b", "#ffd635", "#666fff"];
var darkalbert = ["#541F14", "#626266", "#4B490B", "#001B21", "#5E5656"];

keyPressCount = 0;

function mouseClicked() {

    if (keyPressCount % 2 === 0) {
        var ecraft = getRandomInt(0, 4);
        props.ende = darkalbert[ecraft];
        keyPressCount++;
        //        save to png aka lazy screencapture
        //		  saveCanvas("tapio-neu_"+keyPressCount, 'png');
    } else {

        var acraft = getRandomInt(0, 4);
        props.anfang = fancynancy[acraft];
        keyPressCount++;
    }
    return false; // prevent default
}